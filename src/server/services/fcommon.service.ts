import { Context, ServiceSchema } from 'moleculer';
import _ from 'lodash';
import commonLoader from '../loaders';

const FCommonService: ServiceSchema = {

  name: 'fcommon',
  state: '',

	/**
	 * Service settings
	 */
  settings: {
    batchTime: 1000,
    payloadOptions: {
      debug: false,
      shared: false,
    },
  },

	/**
	 * Service dependencies
	 */
  dependencies: [],

	/**
	 * Actions
	 */
  actions: {
    health(ctx: Context) {
      const state = this.state as unknown as string;
      const content = {
        state,
        status: state === 'up' ? 200 : 503,
        uptime: process.uptime(),
        timestamp: Date.now(),
      };

      return content;
    },
  },

	/**
	 * Events
	 */
  events: {

  },

	/**
	 * Methods
	 */
  methods: {

  },

	/**
	 * Service created lifecycle event handler
	 */
  async created() {
    this.state = 'starting';

    await commonLoader.init(this.logger);
  },

	/**
	 * Service started lifecycle event handler
	 */
  async started() {
    const { logger } = this;
    this.state = 'up';
    logger.info(`FCommon is running.`);

    return this.Promise.resolve();
  },

  stopping() {
    this.state = 'stopping';

    return this.Promise.resolve();
  },

  /**
   * Service stopped lifecycle event handler
   */
  stopped() {
    this.state = 'down';

    return this.Promise.resolve();
  },
};

export = FCommonService;
