import connectToMongoDb from './mongoose.loader';
import { LoggerInstance } from 'moleculer';

class Loader {

  async init(logger: LoggerInstance): Promise<void> {
    await connectToMongoDb(logger);
  }

}

const commonLoader = new Loader();

export default commonLoader;
