import mongoose from 'mongoose';
import { LoggerInstance } from 'moleculer';
import { toBool, getOsEnv } from '../lib/env';

const MONGO_SETTINGS = {
  uri: getOsEnv('MONGO_URI'),
  debug: toBool(getOsEnv('MONGOOSE_DEBUG')),
};

const connectToMongoDb = async (logger: LoggerInstance): Promise<any> => {
  const mongoUri = MONGO_SETTINGS.uri;
  const debug = MONGO_SETTINGS.debug;

  const options = {
    useNewUrlParser: true,
    autoIndex: false, // Don't build indexes
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return err,ors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  };

  return new Promise((resolve, reject) => {
    if (mongoose.connection.readyState === 1) {
      return resolve(mongoose.connection);
    }

    mongoose.connect(mongoUri, options);

    // Exit application on error
    mongoose.connection.on('error', err => {
      logger.error('MongoDB connection error:', err);

      process.exit(1);

      return reject(err);
    });

    // when the connection is connected
    mongoose.connection.on('connected', () => {
      logger.info('MongoDB event connected');

      return resolve(mongoose.connection);
    });

    mongoose.set('useCreateIndex', true);
    mongoose.set('debug', debug);
    mongoose.set('useFindAndModify', false);
  });
};

export default connectToMongoDb;
