import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as path from 'path';
import * as pkg from '../../package.json';

/**
 * Load .env file or for tests the .env.test file.
 */
const ENV_NAME = (process.env.ENV_NAME || 'production').toLowerCase();
const dotenvPath = path.join(__dirname, '../../config-files/env');

// https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
const dotenvFiles = [
  `${dotenvPath}/.env.${ENV_NAME}`,
  `${dotenvPath}/.env.${ENV_NAME}.local`,
];

dotenv.config();
dotenvFiles.forEach((dotenvFile: string) => {
  if (fs.existsSync(dotenvFile)) {
    const envConfig = dotenv.parse(fs.readFileSync(dotenvFile));

    // tslint:disable-next-line: forin
    for (const k in envConfig) {
      process.env[k] = envConfig[k];
    }
  }
});

const NODE_ENV = process.env.NODE_ENV || 'development';

process.env.PACKAGE_VERSION = pkg.version;
process.env.NODE_ENV = NODE_ENV;
console.log('Running App version: ' + ENV_NAME);
console.log('NODE_ENV: ' + NODE_ENV);

const isProduction = Boolean(process.env.PRODUCTION === 'true');

export const env = {
  node: process.env.NODE_ENV,
  envName: ENV_NAME,
  isProduction,
  isTest: process.env.NODE_ENV === 'test',
  isDevelopment: process.env.NODE_ENV === 'development',
};
