import * as path from 'path';
import * as fs from 'fs';
import { Logger } from 'moleculer';
import winston, { format } from 'winston';
import { Loggly } from 'winston-loggly-bulk';
import DailyRotateFile from 'winston-daily-rotate-file';
import util from 'util';
import { TransformableInfo } from 'logform';
import { stringify } from 'flatted/cjs';
import _ from 'lodash';
import { getOsEnv, toBool, getOsEnvOptional } from './env';

const { extend } = Logger;
const envName = process.env.ENV_NAME;
const nodeEnv = process.env.NODE_ENV;
const appName = process.env.APP_NAME;
const LOG_SETTINGS = {
  level: getOsEnv('LOG_LEVEL'),
  consoleLevel: getOsEnv('LOG_CONSOLE_LEVEL'),
  fileLevel: getOsEnv('LOG_FILE_LEVEL'),
  json: toBool(getOsEnvOptional('LOG_JSON')),
  output: getOsEnv('LOG_OUTPUT'),
};

const logDirectory = process.env.LOG_DIRECTORY || path.join(__dirname, '../../../config-files/logs');
const logFileName = process.env.LOG_FILENAME || `${appName}.log.txt`;
const label = process.env.LOG_LABEL || 'fcommon';

const winstonLevelToLogglyLevel = {
  silly: 'silly',
  verbose: 'verbose',
  info: 'info',
  debug: 'debug',
  warn: 'warning',
  error: 'error',
};

/**
 * @param {Error} error
 */
const errorHandler = (error: any) => {
  console.log(error);
};

/**
 * @param {{}} info
 * @param {string} info.level
 * @return {{}}
 */
const prepareMeta = (info: any) => {
  const extra = Object.assign({}, info);
  delete extra.message;
  delete extra.level;
  delete extra.tags;

  const error = info.message instanceof Error ? info.message : new Error(info.message);
  extra.stackError = error.stack;

  return {
    extra,
    level: winstonLevelToLogglyLevel[info.level],
    tags: info.tags || {},
  };
};

// Ignore log messages if they have { private: true }
const ignorePrivate = format((info, opts) => {
  if (info.private) { return false; }
  return info;
});

const printfFormat = format.printf((info: TransformableInfo) => {
  // tslint:disable-next-line:typedef
  const { timestamp, level, label, message, ...args } = info;
  const ts = timestamp.replace(/T/, ' ').replace(/Z/, '');
  let printed = message;

  if (info.splat) {
    printed = util.format(message, ...info.splat);

    return `[${ts}] [${level}]: ${printed}`;
  }

  let extraError = '';
  const numOfArgs = Object.keys(args).length;
  if (numOfArgs > 10) {
    extraError = _.toArray(args).join('');
  } else if (numOfArgs > 0) {
    extraError = stringify(args, null, 2);
  }

  return `[${ts}] [${label}] [${level}]: ${printed} ${extraError}`;
});

const consoleFormat = format.combine(
  format.colorize(),
  format.timestamp(),
  format.label({ label }),
  ignorePrivate(),
  format.splat(),
  format.json(),
  printfFormat,
);

// const logglyFormat = format.combine(
//   format.timestamp(),
//   format.label({ label }),
//   ignorePrivate(),
//   format.splat(),
//   format.json(),
//   printfFormat,
// );

// const fileTransport = new winston.transports.File({
//   filename: path.join(logDirectory, logFileName),
//   level: env.log.fileLevel,
//   format: consoleFormat,
// });

const dailyRotateFileTransport = new DailyRotateFile({
  filename: `${logDirectory}/%DATE%-${logFileName}`,
  datePattern: 'YYYY-MM-DD',
  level: LOG_SETTINGS.fileLevel,
  format: consoleFormat,
});

const consoleTransport = new winston.transports.Console({
  level: LOG_SETTINGS.consoleLevel,
  stderrLevels: ['error'],
  format: consoleFormat,
});

// const logglyTransport = new Loggly({
//   level: 'warn',
//   // level: 'info',
//   token: 'fd9f695d-b0f9-4bc0-b118-xxxx',
//   subdomain: 'fx-800',
//   tags: ['fx-800', appName],
//   stripColors: true,
//   json: true,
//   format: logglyFormat,
// });

const transports = [
  consoleTransport,
  dailyRotateFileTransport,
  // fileTransport,
  // logglyTransport,
];

// if (nodeEnv === 'production') { }

const options: winston.LoggerOptions = {
  level: LOG_SETTINGS.level,
  transports,
};

const winstonLogger = winston.createLogger(options);

// // create a stream object with a 'write' function that will be used by `morgan`
// winstonLogger.stream = {
//   write: (message: any) {
//     // use the 'info' log level so the output will be picked up by both transports (file and console)
//     winstonLogger.verbose(message);
//   },
// };

const logger = (bindings: any) => extend(winstonLogger);

export default logger;
