node {
    def app

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */
        checkout scm
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */

        app = docker.build("fxtrading/fcommon","--build-arg SECRETS_HOST=${env.SECRETS_HOST} --build-arg CI=${env.CI} .")
    }

    stage('Test image') {
        /* Ideally, we would run a test framework against our image.
         * For this example, we're using a Volkswagen-type approach ;-) */

        app.inside {
            sh 'echo "Tests passed"'
        }
    }

    stage('Push image') {
        getVersion()
    }

}

def getVersion() {
    sh '''# Version key/value should be on his own line
        PACKAGE_VERSION=$(cat package.json \\
          | grep version \\
          | head -1 \\
          | awk -F: \'{ print $2 }\' \\
          | sed \'s/[",]//g\')

        echo $PACKAGE_VERSION > version.txt '''

    script {
        env.VERSION = readFile 'version.txt'
    }
}
