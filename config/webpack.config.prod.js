'use strict';

const autoprefixer = require('autoprefixer');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const paths = require('./paths');
const getClientEnvironment = require('./env');
const Dotenv = require('dotenv-webpack');

// Webpack uses `publicPath` to determine where the app is being served from.
// It requires a trailing slash, or the file assets will get an incorrect path.
const publicPath = paths.servedPath;
// Some apps do not use client-side routing with pushState.
// For these, "homepage" can be set to "." to enable relative asset paths.
const shouldUseRelativeAssetPaths = publicPath === './';
// Source maps are resource heavy and can cause out of memory issue for large source files.
const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== 'false';
// `publicUrl` is just like `publicPath`, but we will provide it to our app
// as %PUBLIC_URL% in `index.html` and `process.env.PUBLIC_URL` in JavaScript.
// Omit trailing slash as %PUBLIC_URL%/xyz looks better than %PUBLIC_URL%xyz.
const publicUrl = publicPath.slice(0, -1);
// Get environment variables to inject into our app.
const env = getClientEnvironment(publicUrl);

const isDevBuild = env.stringified['process.env'].NODE_ENV !== '"production"';
// Assert this just to be safe.
// Development builds of React are slow and not intended for production.
// if (env.stringified['process.env'].NODE_ENV !== '"production"') {
//   throw new Error('Production builds must have NODE_ENV=production.');
// }

// Note: defined here because it will be used more than once.
const cssFilename = 'static/css/[name].[contenthash:8].css';

// ExtractTextPlugin expects the build output to be flat.
// (See https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/27)
// However, our output is structured with css, js and media folders.
// To have this structure working with relative paths, we have to use custom options.
const extractTextPluginOptions = shouldUseRelativeAssetPaths
  ? // Making sure that the publicPath goes back to to build folder.
  { publicPath: Array(cssFilename.split('/').length).join('../') }
  : {};

const extractCSS = new ExtractTextPlugin(cssFilename);
const isBrowser = process.env.BROWSER === 'true';
const browserOutput = {
  filename: "bundle.broswer.js",
  path: path.resolve(__dirname, '../dist'),
  library: 'fcommon',
  libraryTarget: 'umd'
};
const serverOutput = {
  filename: "bundle.js",
  path: path.resolve(__dirname, '../dist'),
  libraryTarget: 'umd'
};

// This is the production configuration.
// It compiles slowly and is focused on producing a fast and minimal bundle.
// The development configuration is different and lives in a separate file.
module.exports = {
  // mode: 'production',
  mode: "development",
  // Don't attempt to continue if there are any errors.
  bail: true,
  // We generate sourcemaps in production. This is slow but gives good results.
  // You can exclude the *.map files from the build during deployment.
  devtool: shouldUseSourceMap ? 'source-map' : false,
  // In production, we only want to load the polyfills and the app code.
  entry: "./src/index.ts",
  target: isBrowser ? 'web' : 'node',
  output: isBrowser ? browserOutput : serverOutput,
  resolve: {
    // This allows you to set a fallback for where Webpack should look for modules.
    // We placed these paths second because we want `node_modules` to "win"
    // if there are any conflicts. This matches Node resolution mechanism.
    // https://github.com/facebookincubator/create-react-app/issues/253
    modules: ['node_modules', paths.appNodeModules].concat(
      // It is guaranteed to exist because we tweak it in `env.js`
      process.env.NODE_PATH.split(path.delimiter).filter(Boolean)
    ),
    // These are the reasonable defaults supported by the Node ecosystem.
    // We also include JSX as a common component filename extension to support
    // some tools, although we do not recommend using it, see:
    // https://github.com/facebookincubator/create-react-app/issues/290
    // `web` extension prefixes have been added for better support
    // for React Native Web.
    extensions: [".ts", ".tsx", ".js"],
    alias: {
      'ejs': 'ejs/ejs.min.js'
    },
    plugins: [
      // // Prevents users from importing files from outside of src/ (or node_modules/).
      // // This often causes confusion because we only process files within src/ with babel.
      // // To fix this, we prevent you from importing files out of src/ -- if you'd like to,
      // // please link the files into your node_modules/ and let module-resolution kick in.
      // // Make sure your source files are compiled, as they will not be processed in any way.
      // new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson]),
      // new TsconfigPathsPlugin({ configFile: paths.appTsConfig }),
    ],
  },
  module: {
    strictExportPresence: true,
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          // transpileOnly: true,
          // experimentalWatchApi: true,
        },
      },
      {
        test: require.resolve('lodash'),
        use: [
          {
            loader: 'expose-loader',
            options: 'lodash'
          },
          {
            loader: 'expose-loader',
            options: '_'
          }
        ]
      },
    ],
  },
  plugins: [
    // Moment.js is an extremely popular library that bundles large locale files
    // by default due to how Webpack interprets its code. This is a practical
    // solution that requires the user to opt into importing specific locales.
    // https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
    // You can remove this if you don't use Moment.js:
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

    // extractCSS,

    new Dotenv({
      // path: envFile,
      safe: false,
      systemvars: true,
      silent: false
    }),


    // Makes some environment variables available to the JS code, for example:
    // if (process.env.NODE_ENV === 'production') { ... }. See `./env.js`.
    // It is absolutely essential that NODE_ENV was set to production here.
    // Otherwise React will be compiled in the very slow development mode.
    new webpack.DefinePlugin(env.stringified),

    // // Minify the code.
    // new UglifyJsPlugin({
    //   uglifyOptions: {
    //     parse: {
    //       // we want uglify-js to parse ecma 8 code. However we want it to output
    //       // ecma 5 compliant code, to avoid issues with older browsers, this is
    //       // whey we put `ecma: 5` to the compress and output section
    //       // https://github.com/facebook/create-react-app/pull/4234
    //       ecma: 8,
    //     },
    //     compress: {
    //       ecma: 5,
    //       warnings: false,
    //       // Disabled because of an issue with Uglify breaking seemingly valid code:
    //       // https://github.com/facebook/create-react-app/issues/2376
    //       // Pending further investigation:
    //       // https://github.com/mishoo/UglifyJS2/issues/2011
    //       comparisons: false,
    //     },
    //     mangle: {
    //       safari10: true,
    //     },
    //     output: {
    //       ecma: 5,
    //       comments: false,
    //       // Turned on because emoji and regex is not minified properly using default
    //       // https://github.com/facebook/create-react-app/issues/2488
    //       ascii_only: true,
    //     },
    //   },
    //   // Use multi-process parallel running to improve the build speed
    //   // Default number of concurrent runs: os.cpus().length - 1
    //   parallel: true,
    //   // Enable file caching
    //   cache: true,
    //   sourceMap: shouldUseSourceMap,
    // }),    // Note: this won't work without ExtractTextPlugin.extract(..) in `loaders`.

    // Perform type checking and linting in a separate process to speed up compilation
    new ForkTsCheckerWebpackPlugin({
      async: false,
      tsconfig: paths.appTsProdConfig,
      tslint: paths.appTsLint,
    }),

    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

    new CleanWebpackPlugin(['dist']),

    // new BundleAnalyzerPlugin()

  ],
  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
};

