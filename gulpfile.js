'use strict'

const gulp = require('gulp');
const del = require('del');
const isDev = process.env.NODE_ENV !== 'production';
const targetDir = 'dist';


gulp.task('clean', function () {
  return del([
    '.tmp-dist',
    'dist',
    'coverage'
  ]);
});

gulp.task('copy', function () {
  return gulp.src([
    '.tmp-dist/src/**/*',
    '.tmp-dist/package.json',
  ])
    .pipe(gulp.dest(targetDir));
});

gulp.task('copyAssest', function () {
  return gulp.src([
    'src/public/**/*',
    'src/npm-patches/**/*',
  ], { base: 'src' })
    .pipe(gulp.dest(targetDir));
});

gulp.task('deploy', ['copy', 'copyAssest'], function () {

  return gulp.src([
  ], { base: 'src' })
    .pipe(gulp.dest(targetDir));
});

// start tasks
gulp.task('default', ['deploy']);
