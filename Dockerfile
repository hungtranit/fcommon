### BASE
FROM node:10.16.0-jessie AS base

# Set the working directory
WORKDIR /app

# Copy project specification and dependencies lock files
COPY package.json yarn.lock ./
ADD src/npm-patches /app/src/npm-patches
ADD types /app/types

# Install yarn
RUN npm install -g yarn
RUN apt-get update -y && apt-get install -y libusb-1.0-0 libusb-1.0-0-dev libudev-dev

### DEPENDENCIES
FROM base AS dependencies

# Install Node.js dependencies (only production)
RUN yarn --production
# Copy production dependencies aside
RUN cp -R node_modules /tmp/node_modules
# Install ALL Node.js dependencies
RUN yarn

### TEST & BUILD
FROM dependencies AS build
COPY . ./
ARG CI
# RUN echo $CI

# Run linters and tests
# RUN yarn lint && yarn test
# RUN yarn lint
# RUN yarn test:coverage

# Build Api
RUN yarn build
RUN cp -R dist /tmp/dist

### RELEASE
FROM node:10.16.0-jessie

# Set the working directory
WORKDIR /app

# Copy app sources
COPY . ./

# Copy production dependencies
COPY --from=dependencies /tmp/node_modules ./node_modules
COPY --from=build /tmp/dist ./dist

# Expose application port
EXPOSE 5203

# In production environment
ENV ENV_NAME production
ENV NODE_ENV production
ENV HOST 0.0.0.0
VOLUME ["/app/config-files"]

# Run
CMD ["npm", "start"]
