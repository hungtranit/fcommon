Requirements:
 * Node v8
 - Typescript 2.9 npm install -g typescript
 - Yarn npm install -g yarn


Features:
 * Watch mode - server auto-restarts when code changes
 * Koa v2 with #beautiful async functions

Included middleware:
 * koa-router
 * Custom logger with pretty + JSON modes (based on `NODE_ENV`)

## Getting Started

```
yarn
yarn start
```

## Testing

Testing is powered by Jest. This project also uses supertest for demonstrating a simple routing smoke test suite. Feel free to remove supertest entirely if you don't wish to use it.

Start the test runner in watch mode with:

```sh
yarn test
```

You can also generate coverage with:

```sh
yarn test --coverage
```

## VS Code

prettier-vscode can be installed using the extension sidebar. Search for Prettier - Code formatter. It can also be installed using ext install prettier-vscode in the command palette. Check its repository for configuration and shortcuts.

yarn global add prettier
npm install --global prettier
npm install -g prettier-eslint
yarn add --dev prettier eslint-plugin-prettier
